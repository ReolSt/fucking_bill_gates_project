
// Smart_FMMSView.h : CSmart_FMMSView 클래스의 인터페이스
//

#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "afxdtctl.h"
class CClosureInfo;

class CSmart_FMMSView : public CRecordView
{
protected: // serialization에서만 만들어집니다.
	CSmart_FMMSView();
	DECLARE_DYNCREATE(CSmart_FMMSView)

public:
#ifdef AFX_DESIGN_TIME
	enum{ IDD = IDD_SMART_FMMS_FORM };
#endif
	CInstalledEquipmentInfo* m_ieSet;
	CEnterLog* m_entSet;
	CEventLog *m_evtSet;
	CClosureEquipmentInfo* m_ceSet;
	CClosureInfo* m_ciSet;
	CClosureState* m_csSet;
// 특성입니다.
public:
	CSmart_FMMSDoc* GetDocument() const;

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual CRecordset* OnGetRecordset();
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 구현입니다.
public:
	virtual ~CSmart_FMMSView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_DBView;
	afx_msg void OnBnClickedInstalledButton();
	afx_msg void OnBnClickedEventlogButton();
	afx_msg void OnBnClickedEnterlogButton();
	afx_msg void OnBnClickedEquipmentButton();
	void UpdateDBView();
	CDateTimeCtrl m_CompletionDate;
	afx_msg void OnBnClickedAdd();
	afx_msg void OnBnClickedEdit();
	afx_msg void OnBnClickedDelete();
	afx_msg void OnBnClickedLoad();
	CButton m_Availability;
	afx_msg void OnLvnItemchangedDbview(NMHDR *pNMHDR, LRESULT *pResult);
	int m_index;
	afx_msg void OnBnClickedStateButton();
};

#ifndef _DEBUG  // Smart_FMMSView.cpp의 디버그 버전
inline CSmart_FMMSDoc* CSmart_FMMSView::GetDocument() const
   { return reinterpret_cast<CSmart_FMMSDoc*>(m_pDocument); }
#endif

