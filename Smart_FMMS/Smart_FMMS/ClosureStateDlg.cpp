// StateDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Smart_FMMS.h"
#include "ClosureStateDlg.h"
#include "afxdialogex.h"


// StateDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CClosureStateDlg, CDialogEx)

CClosureStateDlg::CClosureStateDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_CLOSURESTATE_DIALOG, pParent)
	, _inited(false)
{

}

CClosureStateDlg::~CClosureStateDlg()
{
}

void CClosureStateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_DBView);
}


BEGIN_MESSAGE_MAP(CClosureStateDlg, CDialogEx)
	ON_BN_CLICKED(IDC_LOAD, &CClosureStateDlg::OnBnClickedLoad)
END_MESSAGE_MAP()


// ClosureStateDlg 메시지 처리기입니다.


BOOL CClosureStateDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.
	UpdateDBView();
	m_DBView.SetExtendedStyle(LVS_EX_FULLROWSELECT);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CClosureStateDlg::UpdateDBView() {
	static short int width = 100;
	static LPCTSTR name[] =
	{ _T("함체위치명"),_T("Door1"),_T("Door2"),_T("TCP/IP"),_T("RF"),
		_T("온도"),_T("습도"),_T("사이렌"),_T("센서") };
	if (!_inited) {
		for (int i = 0; i < 9; i++) {
			m_DBView.InsertColumn(i + 1, name[i], LVCFMT_LEFT, width);
		}
		_inited = 1;
	}
	m_DBView.DeleteAllItems();
	CClosureState closure_state(m_csSet->m_pDatabase);
	closure_state.Open(CRecordset::dynaset, closure_state.GetDefaultSQL());
	for (int i = 0; !closure_state.IsEOF(); closure_state.MoveNext(),i++) {
		m_DBView.InsertItem(i,closure_state.m_LocationName);
		CString cond;
		cond = closure_state.m_Door1 ? _T("Yes") : _T("No");
		m_DBView.SetItemText(i, 1, cond);
		cond = closure_state.m_Door2 ? _T("Yes") : _T("No");
		m_DBView.SetItemText(i, 2, cond);
		cond = closure_state.m_TCPIP ? _T("Yes") : _T("No");
		m_DBView.SetItemText(i, 3, cond);
		cond = closure_state.m_RF ? _T("Yes") : _T("No");
		m_DBView.SetItemText(i, 4, cond);
		m_DBView.SetItemText(i, 5, closure_state.m_Temerature);
		m_DBView.SetItemText(i, 6, closure_state.m_Humidity);
		cond = closure_state.m_Siren ? _T("Yes") : _T("No");
		m_DBView.SetItemText(i, 7, cond);
		cond = closure_state.m_Sensor ? _T("Yes") : _T("No");
		m_DBView.SetItemText(i, 8, cond);
	}
	closure_state.Close();
}


void CClosureStateDlg::OnBnClickedLoad()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateDBView();
}
